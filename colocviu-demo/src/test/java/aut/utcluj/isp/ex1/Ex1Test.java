/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aut.utcluj.isp.ex1;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mihai.hulea
 */

public class Ex1Test {
    
    public Ex1Test() {
    }

    @Test
    public void testAdd() {
        System.out.println("add");
        int k = 1;
        int z = 3;
        int expResult = 4;
        int result = Ex1.add(k, z);
        assertEquals(expResult, result);
    }
    
}
