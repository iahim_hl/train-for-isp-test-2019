/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aut.utcluj.isp;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author mihai.hulea
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    aut.utcluj.isp.ex1.Ex1Test.class,
    aut.utcluj.isp.ex2.Ex2Test.class,
    aut.utcluj.isp.ex3.Ex3Test.class
})
public class ColocviuTestSuite {
    
}
