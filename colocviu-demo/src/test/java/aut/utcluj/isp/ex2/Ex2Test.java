/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aut.utcluj.isp.ex2;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mihai.hulea
 */
public class Ex2Test {
    
    public Ex2Test() {
    }

    @Test
    public void testConcat() {
        System.out.println("concat");
        String word1 = "abc";
        String word2 = "123";
        Ex2 instance = new Ex2();
        String expResult = "abc123";
        String result = instance.concat(word1, word2);
        assertEquals(expResult, result);
        //fail("The test case is a prototype.");
    }
    
}
